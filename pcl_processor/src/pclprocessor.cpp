#include "pclprocessor.h"

PCLProcessor::PCLProcessor()
{

}

PCLProcessor::~PCLProcessor()
{

}

//Crops the input cloud.
void PCLProcessor::crop(PointCloudT::Ptr &cloud, double x_min, double x_max, double y_min, double y_max, double z_min, double z_max)
{
    std::cout << "size before cropping: " << cloud->points.size() << std::endl;
    pcl::PassThrough<PointT> ptfilter (true);
    ptfilter.setInputCloud (cloud);
    //Filter X
    ptfilter.setFilterFieldName ("x");
    ptfilter.setFilterLimits (x_min, x_max);
    ptfilter.filter (*cloud);
    // Filter Y
    ptfilter.setFilterFieldName ("y");
    ptfilter.setFilterLimits (y_min, y_max);
    ptfilter.filter (*cloud);
    // Filter Z
    ptfilter.setFilterFieldName ("z");
    ptfilter.setFilterLimits (z_min, z_max);
    ptfilter.filter (*cloud);
    std::cout << "size after cropping: " << cloud->points.size() << std::endl;

}
void PCLProcessor::downsample(PointCloudT::Ptr &cloud, double leafsize)
{
    pcl::ApproximateVoxelGrid<PointT> downsampler;
    downsampler.setDownsampleAllData(false);
    downsampler.setInputCloud (cloud);
    downsampler.setLeafSize (leafsize, leafsize, leafsize);
    downsampler.filter (*cloud);
}
void PCLProcessor::removeOutliers(PointCloudT::Ptr &cloud, int meanK, double threshold)
{
    pcl::StatisticalOutlierRemoval<PointT> sor;
    sor.setInputCloud (cloud);
    sor.setMeanK (meanK);
    sor.setStddevMulThresh (threshold);
    sor.setKeepOrganized(true);
    sor.filter (*cloud);
}
void PCLProcessor::fitPlane(PointCloudT::Ptr &cloud, PointCloudT::Ptr &cloud_plane, double threshold, Eigen::VectorXf &plane_coef, int &inlier_count)
{

    // Fit a plane with RANSAC:
    pcl::SampleConsensusModelPlane<PointT>::Ptr
            model_plane (new pcl::SampleConsensusModelPlane<PointT> (cloud));
    pcl::RandomSampleConsensus<PointT> ransac (model_plane);
    std::vector<int> inliers;

    ransac.setDistanceThreshold (threshold);
    ransac.computeModel();
    ransac.getInliers(inliers);



    pcl::copyPointCloud<PointT>(*cloud, inliers, *cloud_plane);
    inlier_count = inliers.size();
    ransac.getModelCoefficients(plane_coef);

    //*/

    /*
    // Create the segmentation object
    pcl::SACSegmentation<PointT> seg;
    // Optional
    seg.setOptimizeCoefficients (false);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (1000);


    seg.setDistanceThreshold (threshold);



    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices<PointT> extract;
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    extract.setInputCloud (cloud);
    seg.setInputCloud(cloud);
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
    seg.segment (*inliers, *coefficients);
    extract.setIndices (inliers);
    extract.setNegative (false);


    // Get the points associated with the planar surface
    extract.filter (*cloud_plane);


    PointCloudT::Ptr cloud_out;
    cloud_out.reset (new PointCloudT);
    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_out);

    cloud = cloud_out;
    */


}
void PCLProcessor::removeNanPoints(PointCloudT::Ptr &cloud)
{
    std::vector<int> inliers;
    pcl::removeNaNFromPointCloud(*cloud, inliers);
    pcl::removeNaNFromPointCloud(*cloud, *cloud, inliers);
}
void PCLProcessor::cropFromPlane(PointCloudT::Ptr &cloud, Eigen::VectorXf &plane_coef, Eigen::Matrix4f &transform_out, Eigen::Vector3f &vec, double &c_o, double &s_o)
{


    // Here we transform all points so that the fitted plane is parallel to the camera.
    // This is done using EAA transformations.
    // Then the data is cropped based on some standard measurements.

    PointCloudT::Ptr transformed_cloud;
    transformed_cloud.reset(new PointCloudT);
    // Create transformation matrix using rodrigues rotation formula (EAA)
    Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
    // First we find the rotation axis by crossing two vectors.
    Eigen::Vector3f rot_axis, vecA, vecB;
    vecA = plane_coef.head(3);
    if (vecA(2) < 0)
        vecA *= -1;

    vecB << 0, 0, 1;

    rot_axis = vecA.cross(vecB);
    rot_axis.normalize();


    // Find cosine + sine of angle:
    double c = vecA.dot(vecB) / (vecA.norm()*vecB.norm()); // norm of both should be 1, so maybe omit.
    double s = std::sin(std::acos(c));

    // Fill rotation matrix with rodriques formula:
    transform(0,0) = rot_axis(0) * rot_axis(0) * (1-c) + c;
    transform(0,1) = rot_axis(0) * rot_axis(1) * (1-c) - rot_axis(2)*s;
    transform(0,2) = rot_axis(0) * rot_axis(2) * (1-c) + rot_axis(1)*s;

    transform(1,0) = rot_axis(0) * rot_axis(1) * (1-c) + rot_axis(2)*s;
    transform(1,1) = rot_axis(1) * rot_axis(1) * (1-c) + c;
    transform(1,2) = rot_axis(1) * rot_axis(2) * (1-c) - rot_axis(0)*s;

    transform(2,0) = rot_axis(0) * rot_axis(2) * (1-c) - rot_axis(1)*s;
    transform(2,1) = rot_axis(1) * rot_axis(2) * (1-c) + rot_axis(0)*s;
    transform(2,2) = rot_axis(2) * rot_axis(2) * (1-c) + c;

    transform(3,0) = 0;
    transform(3,1) = 0;
    transform(3,2) = 0;
    transform(3,3) = 1;

    transform_out = transform;
    // Now lets transform the point cloud to do some cropping :D
    pcl::transformPointCloud (*cloud, *transformed_cloud, transform);

    crop(transformed_cloud, -1, 1, -1, 1, 0.5, 0.95);

    pcl::transformPointCloud (*transformed_cloud, *cloud, transform.inverse());
    cloud = transformed_cloud;
    s_o = s;
    c_o = c;
    vec = rot_axis;


    //

}

void PCLProcessor::medianFilter(PointCloudT::Ptr &cloud, unsigned int window_size)
{
    pcl::MedianFilter<PointT> filter;
    PointCloudT::Ptr filtered_cloud;
    filtered_cloud.reset(new PointCloudT);
    removeOutliers(cloud, 3, 1);

    // removeNanPoints(cloud);
    // Remove points with z = 0;
    pcl::PassThrough<PointT> pass;
    pass.setInputCloud (cloud);
    pass.setFilterFieldName ("z");
    pass.setFilterLimits (0.01, 1.5);
    pass.setKeepOrganized(true);
    //pass.setFilterLimitsNegative (true);
    pass.filter (*filtered_cloud);

    //removeOutliers(cloud, 3, 1);

    cloud = filtered_cloud;


    filter.setInputCloud(cloud);
    filter.setWindowSize(window_size);
    filter.setMaxAllowedMovement(1);

    filter.applyFilter(*filtered_cloud);
    cloud = filtered_cloud;
}

void PCLProcessor::removeUncertainPoints(PointCloudT::Ptr &cloud, int threshold)
{
    int height = cloud->height;
    int width = cloud->width;

    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            PointT &current_point = (*cloud)(x,y);
            if (current_point.g > threshold)
            {
                current_point.x = std::numeric_limits<float>::quiet_NaN();
                current_point.y = std::numeric_limits<float>::quiet_NaN();
                current_point.z = std::numeric_limits<float>::quiet_NaN();
                //current_point.g = ;
            }

        }
    }

    //PointT current_point = cloud(3,4);


    /*
    pcl::PassThrough<PointT> pass;
    pass.setInputCloud (cloud);
    pass.setFilterFieldName ("rgb");
    pass.setFilterLimits (0, threshold);

    PointCloudT::Ptr filtered_cloud;
    filtered_cloud.reset(new PointCloudT);
    pass.filter(*filtered_cloud);

    cloud = filtered_cloud;
*/
}


void PCLProcessor::KMeansClustering(PointCloudT::Ptr &input_cloud, PointCloudT::Ptr &cluster1, PointCloudT::Ptr &cluster2, Eigen::Vector3f &centroid1, Eigen::Vector3f &centroid2)
{
    // Try own implementation:
    // Basic kmean should be running before lunch :)
    Eigen::Vector3f centroid1_prev;
    std::vector<int> cluster1_idx;
    std::vector<int> cluster2_idx;

    removeNanPoints(input_cloud);


    centroid1 = Eigen::Vector3f(0.3,0,1);
    centroid2 = Eigen::Vector3f(-0.3,0,1);

    //loop
    for (size_t j = 0; j < 200; j++)
    {

        centroid1_prev = centroid1;
        cluster1_idx.clear();
        cluster2_idx.clear();

        // Assign all points to a cluster:

        omp_lock_t lock1,lock2;

        omp_init_lock(&lock1);
        omp_init_lock(&lock2);
        auto start = std::chrono::steady_clock::now();
#pragma omp parallel for num_threads(2)
        for (size_t i = 0; i < input_cloud->size(); i++)
        {

            Eigen::Vector3f current_point = (*input_cloud)[i].getVector3fMap();
            if ((centroid1 - current_point).norm() < (centroid2 - current_point).norm())
            {
                omp_set_lock(&lock1);
                cluster1_idx.push_back(i);
                omp_unset_lock(&lock1);
            }
            else
            {
                omp_set_lock(&lock2);
                cluster2_idx.push_back(i);
                omp_unset_lock(&lock2);
            }

        }
        auto end = std::chrono::steady_clock::now();

        auto diff = end - start;
        //std::cout << "Checking distances took: " << std::chrono::duration <double, std::milli> (diff).count() << " ms" << std::endl;

        // Update cluster centroids
        centroid1 = Eigen::Vector3f(0,0,0);
        centroid2 = Eigen::Vector3f(0,0,0);

#pragma omp parallel sections
        {
#pragma omp section
            {
            for (size_t i = 0; i < cluster1_idx.size(); i++)
            {
                Eigen::Vector3f current_point = (*input_cloud)[cluster1_idx[i]].getVector3fMap();
                centroid1 += current_point;
            }
            centroid1 = centroid1 / cluster1_idx.size();

            }
#pragma omp section
            {
            for (size_t i = 0; i < cluster2_idx.size(); i++)
            {
                Eigen::Vector3f current_point = (*input_cloud)[cluster2_idx[i]].getVector3fMap();
                centroid2 += current_point;
            }
            centroid2 = centroid2 / cluster2_idx.size();

            }
        }

        if ((centroid1 - centroid1_prev).norm() < 0.001)
        {
            std::cout << "Clusters found after " << j << " iterations. " << std::endl;
            std::cout << "Centroid 1: " << std::endl << centroid1 << std::endl;
            std::cout << "Centroid 2: " << std::endl << centroid2 << std::endl;
            break;
        }
    }
    // Write output to cluster1 and cluster2 pointclouds.
    cluster1.reset(new PointCloudT);
    cluster2.reset(new PointCloudT);
#pragma omp parallel sections
    {
#pragma omp section
        for (size_t i = 0; i < cluster1_idx.size(); i++)
        {
            cluster1->push_back((*input_cloud)[cluster1_idx[i]]);
        }
#pragma omp section
        for (size_t i = 0; i < cluster2_idx.size(); i++)
        {
            cluster2->push_back((*input_cloud)[cluster2_idx[i]]);
        }
    }




}
void PCLProcessor::splitPC(PointCloudT::Ptr &input_cloud, Eigen::Vector3f &cloud_center, Eigen::Vector3f &opposite_center)
{
    Eigen::Vector3f normal_vec = cloud_center - opposite_center;
    std::cout << "Normal vec: \n" << normal_vec << std::endl;
    Eigen::Vector3f point_vec;
    std::cout << "PCsize " << input_cloud->size() <<  std::endl;
    unsigned int size = input_cloud->size();
    std::vector<unsigned int> points_to_remove;

    for (size_t i = size-1; i > 0; i--)
    {
        Eigen::Vector3f current_point = (*input_cloud)[i].getVector3fMap();
        point_vec = current_point - cloud_center;
        //std::cout << point_vec << std::endl;
        double angle = std::acos(normal_vec.dot(point_vec)/(normal_vec.norm()*point_vec.norm()));
        if ((angle < M_PI_2))
        {
            input_cloud->erase(input_cloud->begin()+i);
            //std::cout << "removed a point, angle: " << angle <<  std::endl;
        }

    }


    std::cout << "PCsize after " << input_cloud->size() <<  std::endl;
}

void PCLProcessor::estimateNormals(PointCloudT::Ptr &cloud, pcl::PointCloud<pcl::Normal>::Ptr &cloud_normals)
{
    // This function is based on http://pointclouds.org/documentation/tutorials/normal_estimation.php


    auto start = std::chrono::steady_clock::now();
    // Argument is number of threads. 0 = auto
    pcl::NormalEstimationOMP<PointT, pcl::Normal> normal_estimator;
    normal_estimator.setInputCloud(cloud);
    std::cout << "monkey debyg" << std::endl;
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());
    normal_estimator.setSearchMethod(tree);


    // Use all neighbors in a sphere of radius 3cm
    normal_estimator.setRadiusSearch (0.03);

    // Compute the features
    normal_estimator.compute (*cloud_normals);
    auto end = std::chrono::steady_clock::now();

    auto diff = end - start;
    std::cout << "Normal estimation took: " << std::chrono::duration <double, std::milli> (diff).count() << " ms" << std::endl;
    std::cout << "Went through: " << cloud->points.size() << " points" << std::endl;


}

void PCLProcessor::filterNormals(PointCloudT::Ptr &input_cloud, pcl::PointCloud<pcl::Normal>::Ptr &cloud_normals, PointCloudT::Ptr &output_cloud, Eigen::Vector3f &comparison_vec, double min_angle, double max_angle)
{
    (*output_cloud) = (*input_cloud);
    unsigned int size = output_cloud->points.size();
    if (comparison_vec(2) > 0)
        comparison_vec *= -1;
    // We iterate backwards to avoid problems with removing elements.
    for (size_t i = size-1; i > 0; i--)
    {
        Eigen::Vector3f point_vec = (*cloud_normals)[i].getNormalVector3fMap();
        //std::cout << "angle is: " << std::endl;
        double angle = std::acos(comparison_vec.dot(point_vec)/(comparison_vec.norm()*point_vec.norm()));
        //std::cout << angle << std::endl;
        if (angle > min_angle && angle < max_angle)
        {
            // Do nothing
        }
        else
        {
            // Remove bad point + normal
            output_cloud->erase(output_cloud->begin()+i);
            cloud_normals->erase(cloud_normals->begin()+i);
        }
    }
}


void PCLProcessor::removeZeroPoints(PointCloudT::Ptr &cloud)
{
    unsigned int size = cloud->points.size();
    size_t count = 0;
    // We iterate backwards to avoid problems with removing elements.
    for (size_t i = size-1; i > 0; i--)
    {
        Eigen::Vector3f current_point = (*cloud)[i].getVector3fMap();
        if (current_point.x() == 0 && current_point.y() == 0 && current_point.z() == 0)
        {
            count++;
            cloud->erase(cloud->begin()+i);
            std::cout << "i " << i << std::endl;
        }

    }
    std::cout << "Removed " << count << " points" << std::endl;
}
