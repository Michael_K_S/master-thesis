#ifndef PCLPROC_H
#define PCLPROC_H

#include <omp.h>


#include <iostream>
#include <vector>
#include <random>
#include <cmath>
#include <chrono>

// Point Cloud Library
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/ModelCoefficients.h>

#include <pcl/common/transforms.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/median_filter.h>

//#include <pcl/ml/kmeans.h>

#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>

#include <eigen3/Eigen/Dense>

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

class PCLProcessor
{
public:
    std::minstd_rand0 random; // Random generator
    PCLProcessor();
    ~PCLProcessor();
    void crop(PointCloudT::Ptr &cloud, double x_min, double x_max, double y_min,
              double y_max, double z_min, double z_max);
    void downsample(PointCloudT::Ptr &cloud, double leafsize);
    void removeOutliers(PointCloudT::Ptr &cloud, int meanK, double threshold);
    void fitPlane(PointCloudT::Ptr &cloud, PointCloudT::Ptr &cloud_plane, double threshold, Eigen::VectorXf &plane_coef, int &inlier_count);
    void removeNanPoints(PointCloudT::Ptr &cloud);
    void cropFromPlane(PointCloudT::Ptr &cloud, Eigen::VectorXf &plane_coef,  Eigen::Matrix4f &transform_out, Eigen::Vector3f &vec, double &c_o, double &s_o);
    void medianFilter(PointCloudT::Ptr &cloud, unsigned int window_size);
    void removeUncertainPoints(PointCloudT::Ptr &cloud, int threshold);

    /**
     * @brief KMeansClustering runs K-means clustering n times with K = 2. Returns the results with the lowest sum of in-cluster distances to the centroid.
     * @param input_cloud [in] The cloud to split into 2
     * @param cluster1 [out] Cluster1
     * @param cluster2 [out] Cluster2
     * @param trials [in] the number of times to run the kmeans algorithm before returning
     */
    void KMeansClustering(PointCloudT::Ptr &input_cloud, PointCloudT::Ptr &cluster1, PointCloudT::Ptr &cluster2, Eigen::Vector3f &centroid1, Eigen::Vector3f &centroid2);

    /**
     * @brief splitPC splits the pointcloud in half, using a plane define by normal vector opposite_center-cloud_center.
     * @param input_cloud is the cloud to split in half
     * @param cloud_center is the center of the cloud representing a leg
     * @param opposite_center is the center of the opposite leg
     */
    void splitPC(PointCloudT::Ptr &input_cloud, Eigen::Vector3f &cloud_center, Eigen::Vector3f &opposite_center);

    /**
     * @brief estimateNormals wraps the PCL openMP normal estimator. It works by least square fitting a plane to a small neighbourhood.
     * @param cloud Input and output cloud
     */
    void estimateNormals(PointCloudT::Ptr &cloud, pcl::PointCloud<pcl::Normal>::Ptr &cloud_normals);

    /**
     * @brief filterNormals checks the angle between the normal of each point and the normal vector. Removes point with a normal outside range.
     * @param cloud the input pointcloud
     * @param cloud_normals normals for input cloud
     * @param min_angle  range of angles to accept
     * @param max_angle  range of angles to accept
     */
    void filterNormals(PointCloudT::Ptr &input_cloud, pcl::PointCloud<pcl::Normal>::Ptr &cloud_normals, PointCloudT::Ptr &output_cloud, Eigen::Vector3f &comparison_vec, double min_angle, double max_angle);

    /**
     * @brief removeZeroPoints removes points at origo, since these could break the ransac stuff. very slow unfortunately
     * @param cloud
     */
    void removeZeroPoints(PointCloudT::Ptr &cloud);



};

#endif // PCLPROC_H
