// ROS
#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/passthrough.h>


// Custom messages
#include "caros_robovision/pcl.h"


// STD libs
#include <memory>
#include <iostream>
#include <vector>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloudT;


ros::Publisher* pubptr;
sensor_msgs::PointCloud2 cloud2;

bool getPCL(caros_robovision::pcl::Request &req,
            caros_robovision::pcl::Response &res)
{
    pubptr->publish (cloud2);

    //res.out = true;
    ROS_INFO("Sending fake pcl");
    return true;
}

pcl::PCDReader reader;

int main(int argc, char** argv) {
    // roscpp
    ros::init(argc, argv, "robovision_dummy_node");
    ros::NodeHandle nh;

    PointCloudT::Ptr cloud (new PointCloudT);

    // http://wiki.ros.org/pcl/Overview

    //pcl::io::loadPCDFile("dummy.pcd",cloud2);
    reader.read<pcl::PointXYZ>("test-pointcloud.pcd", *cloud);

    std::cout << cloud->points.size() << std::endl;
    pcl::PassThrough<pcl::PointXYZ> ptfilter (true);
    ptfilter.setInputCloud (cloud);
    // Filter Z
    ptfilter.setFilterFieldName ("z");
    ptfilter.setFilterLimits (0.01, 2);
    ptfilter.filter (*cloud);
    std::cout << cloud->points.size() << std::endl;


    pcl::toROSMsg(*cloud,cloud2);


    ros::Publisher pub = nh.advertise<sensor_msgs::PointCloud2> ("points2", 1);
    pubptr = &pub;

    ROS_INFO("robovision_dummy_node started");


    ros::ServiceServer service = nh.advertiseService("rv/get_pcl", getPCL);
    ros::spinOnce();



    ros::Rate loop_rate(1);

    while(ros::ok())
    {


        ros::spinOnce();
        loop_rate.sleep();
    }


    return 0;



}
