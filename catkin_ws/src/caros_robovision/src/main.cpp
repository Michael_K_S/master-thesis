// ROS
#include <ros/ros.h>
#include "caros_robovision/pcl.h"

#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/passthrough.h>



// Robovision3d
#include <Robovision/RoboVision3d.h>


// STD libs
#include <memory>
#include <iostream>
#include <vector>


// Nice global scope of the robovision sensor
RoboVision3D robo;

// Clouds:
typedef pcl::PointCloud<pcl::PointXYZ> PointCloudT;
PointCloudT::Ptr cloud;
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudrgb;
sensor_msgs::PointCloud2 cloud2;

// Pointer to publisher
ros::Publisher* pubptr;


bool getPCL(caros_robovision::pcl::Request &req,
            caros_robovision::pcl::Response &res)
{

    try {
      robo.startPattern();
      cloudrgb = robo.getPointCloud();

      pcl::PassThrough<pcl::PointXYZRGB> ptfilter (true);
      ptfilter.setInputCloud (cloudrgb);
      // Filter Z
      ptfilter.setFilterFieldName ("z");
      ptfilter.setFilterLimits (0.01, 2);
      ptfilter.filter (*cloudrgb);


      //pcl::copyPointCloud(*cloudrgb,*cloud);
      pcl::toROSMsg(*cloudrgb,cloud2);
      cloud2.header.stamp = ros::Time::now();

      pubptr->publish(cloud2);
    } catch (...) {
      ROS_INFO("PCL acquisition threw exception");
      //res.out = false;
      return false;
    }
    //res.out = true;
    return true;
}


int main(int argc, char** argv) {
    // roscpp
    ros::init(argc, argv, "robovision_node");
    ros::NodeHandle nh;

    //setup exposures
    std::vector<double> exp;
    exp.push_back(0.5);
    exp.push_back(1.0);
    


    robo.setVisualization(false);
    robo.setCalibration("newcalib.yaml");
    robo.setExposure(exp);
    robo.setEncodedImageSmoothing(0.5);
    robo.setNumberOfBits(10);


    robo.setCertaintyType(CERTAINTY_TYPE_BEST_POS);
    robo.setUseFullView(true);
    robo.setIndirectLightRatio(0.1);

    robo.setPointCloudColorOption(
        RoboVision3D::PointCloudColorOption::PC_COLOR_CERTAINTY);
    robo.setCreateOrganizedPointCloud(false);
    robo.setSaveSequence(false);

    robo.initializeSequence();


    // Setup pointclouds:
    cloud.reset(new pcl::PointCloud<pcl::PointXYZ>);
    cloudrgb.reset(new pcl::PointCloud<pcl::PointXYZRGB>);

    // Setup service and publisher
    ros::ServiceServer service = nh.advertiseService("rv/get_pcl", getPCL);

    ros::Publisher pub = nh.advertise<sensor_msgs::PointCloud2> ("points2", 1);
    pubptr = &pub;



    ros::spinOnce();



    ros::Rate loop_rate(1);

    while(ros::ok())
    {
        //ROS_INFO("Inloop robo");
        ros::spinOnce();
        loop_rate.sleep();
    }


    return 0;



}
