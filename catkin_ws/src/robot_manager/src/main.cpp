// ROS
#include <ros/ros.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/Transform.h>
#include <std_msgs/Float64.h>

// Caros deps:
#include <caros_common_msgs/Q.h>

#include <caros_universalrobot/UrServiceServoQ.h>
#include <caros/serial_device_si_proxy.h>
#include <caros/common_robwork.h>
#include <caros_control_msgs/RobotState.h>
#include "robot_manager/changeState.h"


// Other homegrown deps:
#include "robot_manager/robotIsRunning.h"
#include "robot_manager/robotStatus.h"
#include "marker_detection/detectMarker.h"

// Robwork deps:
#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/math/Wrench6D.hpp>


// STD libs
#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>
#include <array>

// utility classes needed
#include "invkin.h"
#include "csvloader.h"
#include "paths.h"


// Debug code:
#include "csvsaver.h"


// Global state vars
geometry_msgs::Transform target_pose;
geometry_msgs::Transform pose_corr_wrench;
geometry_msgs::Transform current_transform;


std::array<rw::math::Vector3D<double>,5> correction;
rw::math::Vector3D<double> integral_error(0.0,0.0,0.0);

geometry_msgs::Wrench target_wrench;
geometry_msgs::Wrench current_wrench;
geometry_msgs::Wrench error_wrench;
geometry_msgs::Wrench offset_wrench;


rw::math::Q current_robot_Q;
enum targetLeg
{
    LEFT_LEG,
    RIGHT_LEG
};
enum state
{
    MOVE_TO_START_PROX,
    MOVE_TO_START_POS,
    PERFORM_SCAN,
    MOVE_TO_STOP_PROX,
    MOVE_TO_HOME,
    CAMERA_AT_MARKER,
    WAIT
};

bool robotIsRunning = false;
bool forceControlIsRunning = false;
bool hasPaths = false;
targetLeg currentLeg = LEFT_LEG;
state currentState = WAIT;
paths currentPaths;
int current_step = 0;
rw::math::Transform3D<double> baseTsensor;
ros::ServiceClient detectMarkerClient;


void current_wrench_callback(const geometry_msgs::WrenchStamped::ConstPtr& wrench)
{
    current_wrench = (*wrench).wrench;
    //ROS_INFO("Updated current wrench");
}


void robot_state_callback(const caros_control_msgs::RobotState state)
{
    if (state.s_stopped || state.e_stopped)
    {
        //ROS_INFO("Robot is safety or emergency stopped");
        //robot_manager::robotIsRunning srv;
        //srv.request.isRunning = false;
        //robotIsRunning_client.call(srv);
    }
}

void coef_callback(const dvt_processing::coef msg)
{
    currentPaths.updateVariables(msg);
    hasPaths = true;
    ROS_INFO("Updated the position path");
}

void robot_q_callback(const caros_control_msgs::RobotState msg)
{
    current_robot_Q = caros::toRw(msg.q);
}


// Service callbacks

bool startProx(robot_manager::changeState::Request &req,
               robot_manager::changeState::Response &res)
{
    currentState = MOVE_TO_START_PROX;
    ROS_INFO("State: MOVE_TO_START_PROX");
    return true;
}

bool startPos(robot_manager::changeState::Request &req,
              robot_manager::changeState::Response &res)
{
    currentState = MOVE_TO_START_POS;
    ROS_INFO("State: MOVE_TO_START_POS");
    return true;
}


bool performScan(robot_manager::changeState::Request &req,
                 robot_manager::changeState::Response &res)
{
    currentState = PERFORM_SCAN;
    current_step = 0;
    ROS_INFO("State: PERFORM_SCAN");
    return true;
}

bool stopProx(robot_manager::changeState::Request &req,
              robot_manager::changeState::Response &res)
{
    currentState = MOVE_TO_STOP_PROX;
    ROS_INFO("State: MOVE_TO_STOP_PROX");
    return true;
}

bool moveToHome(robot_manager::changeState::Request &req,
                robot_manager::changeState::Response &res)
{
    currentState = MOVE_TO_HOME;
    robotIsRunning = true;
    ROS_INFO("State: MOVE_TO_HOME");

    return true;
}
bool cameraAtMarker(robot_manager::changeState::Request &req,
                    robot_manager::changeState::Response &res)
{
    currentState = CAMERA_AT_MARKER;
    robotIsRunning = true;
    ROS_INFO("State: CAMERA_AT_MARKER");
    return true;

}
bool setWrenchOffset(robot_manager::changeState::Request &req,
                     robot_manager::changeState::Response &res)
{
    offset_wrench = current_wrench;
    ROS_INFO("Updated offset wrench to:");
    std::cout << offset_wrench << std::endl;
    return true;
}

bool setsensorTbase(robot_manager::changeState::Request &req,
                    robot_manager::changeState::Response &res)
{
    marker_detection::detectMarker srv_call;

    if(detectMarkerClient.call(srv_call))
    {

        rw::math::RPY<double> flipY(0,M_PI,0);
        rw::math::Transform3D<double> rw_baseTsensor = caros::toRw(srv_call.response.baseTsensor);

        // Remember to add marker to cam transform as well.
        std::cout << "baseTsensor:" << std::endl;
        // Setting some part of the rotation to 0
        rw_baseTsensor.R()(0,2) = 0.0;
        rw_baseTsensor.R()(1,2) = 0.0;
        rw_baseTsensor.R()(2,0) = 0.0;
        rw_baseTsensor.R()(2,1) = 0.0;
        rw_baseTsensor.R()(2,2) = 1.0;


        baseTsensor.R() = flipY.toRotation3D() * rw_baseTsensor.R();
        baseTsensor.P() = rw_baseTsensor.P();
        std::cout << baseTsensor << std::endl;



        return true;
    }

    return false;
}




bool robotIsRunningCallback(robot_manager::robotIsRunning::Request &req,
                            robot_manager::robotIsRunning::Response &res)
{
    robotIsRunning = req.isRunning;
    return true;
}

bool forceControlIsRunningCallback(robot_manager::robotIsRunning::Request &req,
                                   robot_manager::robotIsRunning::Response &res)
{
    forceControlIsRunning = req.isRunning;
    return true;
}


int main(int argc, char** argv) {
    // roscpp
    ros::init(argc, argv, "main_node");
    ros::NodeHandle nh;
    invkin urIK;

    // Set default baseTsensor
    rw::math::Vector3D<double> default_P(-0.740,-0.181,0.95);
    rw::math::RPY<double> default_R(0,0,-M_PI);

    baseTsensor.P() = default_P;
    baseTsensor.R() = default_R.toRotation3D();


    // Load force path
    ForcePath force1("testwrench.csv");

    // Publish current state:
    ros::Publisher pub_status = nh.advertise<robot_manager::robotStatus> ("robotStatus",1);

    // Subscribe to topics
    ros::Subscriber sub_current_wrench = nh.subscribe("/caros_netft/caros_ft_sensor_service_interface/wrench",
                                                      10, current_wrench_callback);
    ros::Subscriber sub_robotstate = nh.subscribe("/caros_universalrobot/caros_serial_device_service_interface/robot_state",
                                                  10, robot_state_callback);
    ros::Subscriber sub_coef = nh.subscribe("coefficients", 1000, coef_callback);

    ros::Subscriber sub_robot_q = nh.subscribe("/caros_universalrobot/caros_serial_device_service_interface/robot_state", 10, robot_q_callback);


    // Advertise services
    ros::ServiceServer startProx_srv = nh.advertiseService("rm/startProx", startProx);
    ros::ServiceServer startPos_srv = nh.advertiseService("rm/startPos", startPos);
    ros::ServiceServer performScan_srv = nh.advertiseService("rm/performScan", performScan);
    ros::ServiceServer stopProx_srv = nh.advertiseService("rm/stopProx", stopProx);
    ros::ServiceServer moveToHome_srv = nh.advertiseService("rm/moveToHome", moveToHome);
    ros::ServiceServer cameraAtMarker_srv = nh.advertiseService("rm/cameraAtMarker", cameraAtMarker);
    ros::ServiceServer robotIsRunning_srv = nh.advertiseService("rm/setRobotIsRunning", robotIsRunningCallback);
    ros::ServiceServer forceControlIsRunning_srv = nh.advertiseService("rm/setForceControlIsRunning", forceControlIsRunningCallback);

    ros::ServiceServer setWrenchOffset_srv = nh.advertiseService("rm/setWrenchOffset", setWrenchOffset);
    ros::ServiceServer updateSensorBaseT_srv = nh.advertiseService("rm/setsensorTbase", setsensorTbase);


    // Set up service client for ur.
    ros::ServiceClient client = nh.serviceClient<caros_universalrobot::UrServiceServoQ>("/caros_universalrobot/servo_q");
    detectMarkerClient = nh.serviceClient<marker_detection::detectMarker>("md/detectMarker");


    caros_universalrobot::UrServiceServoQ servQ;


    rw::math::Q targetQ_rw(6, 1.57, -2.4, 2.4, -1.57, -1.57, 0);


    servQ.request.target.data = targetQ_rw.toStdVector();

    ROS_INFO("Subscribed to a lot of topics.");




    ros::spinOnce();
    int ros_rate = 30;
    ros::Rate loop_rate(ros_rate);
    int steps_per_scan = ros_rate * 10;


    //test code:
    double i = 0;

    // Nice stuff for the force control
    int force_update_divider = 0;
    csvSaver csv30hz("forcedata_z.csv");

    while(ros::ok())

    {
        /* This is the main control loop. Goes like this:
         * - Get pose + pose corrections (control law)
         * - Apply inverse kinematics solver
         * - Send Q to the robot.
         */

        // Publish status:
        robot_manager::robotStatus status_msg;

        if (currentState == WAIT)
            status_msg.waiting = true;
        else
            status_msg.waiting = false;

        status_msg.robotIsRunning = robotIsRunning;
        status_msg.forceControlIsRunning = forceControlIsRunning;
        status_msg.hasPaths = hasPaths;

        pub_status.publish(status_msg);




        if (robotIsRunning)
        {
            switch(currentState)
            {
            case MOVE_TO_START_PROX:

                if (currentLeg == LEFT_LEG)
                    current_transform = currentPaths.getProxPointLeft(0.05, 0);
                else
                    current_transform = currentPaths.getProxPointRight(0.05, 0);
                //currentState = WAIT;
                /*
                current_transform.translation.x = 0.5; // was 0
                current_transform.translation.y = 0;
                current_transform.translation.z = 0.5; // was 1.04

                current_transform.rotation.x = -1; // was 0,0,-1
                current_transform.rotation.y = 0;
                current_transform.rotation.z = 0;
                //*/
                //currentState = WAIT;
                // Debug:
                target_wrench.force.x = 0;
                target_wrench.force.y = 0;
                target_wrench.force.z = -5;
                break;

            case MOVE_TO_STOP_PROX:

                if (currentLeg == LEFT_LEG)
                    current_transform = currentPaths.getProxPointLeft(0.05, 99);
                else
                    current_transform = currentPaths.getProxPointRight(0.05, 99);
                //currentState = WAIT;
                break;

            case MOVE_TO_START_POS:
                current_step = 0;
                if (currentLeg == LEFT_LEG)
                    current_transform = currentPaths.getTransformLeft(current_step);
                else
                    current_transform = currentPaths.getTransformRight(current_step);
                /*
                current_transform.translation.x = 0; // was 0
                current_transform.translation.y = 0;
                current_transform.translation.z = 1.04; // was 1.04

                current_transform.rotation.x = 0; // was 0,0,-1
                current_transform.rotation.y = 0;
                current_transform.rotation.z = -1;
                //*/
                //currentState = WAIT;
                // Debug:
                target_wrench.force.x = 0;
                target_wrench.force.y = 0;
                target_wrench.force.z = -5;


                break;

            case PERFORM_SCAN:

                if (currentLeg == LEFT_LEG)
                    current_transform = currentPaths.getTransformLeft(current_step);
                else
                    current_transform = currentPaths.getTransformRight(current_step);

                target_wrench = force1.getWrench((double)current_step / (double)steps_per_scan, interpType::LINEAR);

                if (current_step < ros_rate*10-1)
                {
                    current_step++;
                }
                if (current_step >= ros_rate*10)
                {
                    currentState = WAIT;
                }
                break;

            case MOVE_TO_HOME:
                robotIsRunning = false;
                targetQ_rw = Q(6, 1.57, -2.4, 2.4, -1.57, -1.57, 0);
                break;

            case CAMERA_AT_MARKER:
                robotIsRunning = false;
                targetQ_rw = Q(6, 0.0, -2.09, 1.74, -2.82, -1.57, 1.57);
                break;
            }


            if (currentState != MOVE_TO_HOME && currentState != CAMERA_AT_MARKER && currentState != WAIT)
            {
                // Calculate orientation of tool based on rodriques rotation formula.
                // Done by aligning normal vector with Z axis of the tool.

                rw::math::Vector3D<> normal_vector;
                rw::math::Vector3D<> ref_vector(0,0,1);
                normal_vector[0] = current_transform.rotation.x;
                normal_vector[1] = current_transform.rotation.y;
                normal_vector[2] = current_transform.rotation.z;



                rw::math::Vector3D<> rotated_normal_vector = baseTsensor.R()*normal_vector;

                rw::math::EAA<double> EAA_rot(ref_vector,normal_vector);

                std::cout << "rotated_normal_vector" << std::endl;
                std::cout << rotated_normal_vector << std::endl;




                rw::math::Transform3D<double> rw_transform = baseTsensor * caros::toRw(current_transform);
                std::cout << "rw_transform" << std::endl;
                std::cout << rw_transform << std::endl;
                rw_transform.R() = EAA_rot.toRotation3D();


                rw::math::Wrench6D<> rw_target_wrench = caros::toRw(target_wrench);
                rw::math::Wrench6D<> rw_current_wrench = caros::toRw(current_wrench);
                rw::math::Wrench6D<> rw_offset_wrench = caros::toRw(offset_wrench);



                rw::math::Wrench6D<> rw_error_wrench = rw_target_wrench - (rw_current_wrench-rw_offset_wrench);

                // Log target and measured wrenches
                std::vector<double> numbers_to_log;

                /*if(forceControlIsRunning)
                    numbers_to_log.push_back(1.0);
                else
                    numbers_to_log.push_back(0.0);

                numbers_to_log.push_back(rw_target_wrench[2]);
                numbers_to_log.push_back(rw_current_wrench[2]-rw_offset_wrench[2]);
                */
                //csv30hz.saveDoubles(numbers_to_log);

                // TODO: This might be to optimistic.
                rw::math::Rotation3D<double> base_to_ft = rw_transform.R();
                //rw::math::Wrench6D<> rw_rotated_error_wrench = base_to_ft * rw_error_wrench;


                rw::math::Vector3D<> weights(0,0,-0.0005);
                rw::math::Vector3D<> error_signal = rw_error_wrench.force();

                rw::math::Vector3D<> weighted_error_signal(
                            error_signal[0] * weights[0],
                        error_signal[1] * weights[1],
                        error_signal[2] * weights[2]);



                std::cout << rw_error_wrench.force() << std::endl;


                std::cout << "error: " << fabs(error_signal[2]) << std::endl;
                if (forceControlIsRunning)
                {
                    if (force_update_divider == 10)
                    {

                        if (fabs(error_signal[2]) > 0)
                            integral_error += weighted_error_signal;

                        if (integral_error.norm2() > 0.05)
                            integral_error *= 0.05/integral_error.norm2();

                        force_update_divider = 0;
                    }
                    force_update_divider++;

                    rw_transform.P() += base_to_ft * integral_error;

                }
                /*numbers_to_log.push_back(rw_transform.P()[0]);
                numbers_to_log.push_back(rw_transform.P()[1]);
                numbers_to_log.push_back(rw_transform.P()[2]);*/

                // get target position
                rw::math::Transform3D<> current_position;
                urIK.solveFK(current_robot_Q,current_position);
                /*std::cout << current_robot_Q << std::endl;
                std::cout << current_position.P() << std::endl;

                numbers_to_log.push_back(current_position.P()[0]);
                numbers_to_log.push_back(current_position.P()[1]);
                numbers_to_log.push_back(current_position.P()[2]);

                csv30hz.saveDoubles(numbers_to_log);
                std::cout << "look here: " << rw_transform.P() << std::endl;
*/



                urIK.solveIK(rw_transform, targetQ_rw);
                std::cout << targetQ_rw << std::endl;

                if(currentState == PERFORM_SCAN && robotIsRunning)
                {
                    for (size_t i = 0; i < 6; i++)
                    {
                        numbers_to_log.push_back(targetQ_rw[i]);
                    }
                    for (size_t i = 0; i < 6; i++)
                    {
                        numbers_to_log.push_back(current_robot_Q[i]);
                    }
                    // Target position
                    numbers_to_log.push_back(rw_transform.P()[0]);
                    numbers_to_log.push_back(rw_transform.P()[1]);
                    numbers_to_log.push_back(rw_transform.P()[2]);

                    numbers_to_log.push_back(current_position.P()[0]);
                    numbers_to_log.push_back(current_position.P()[1]);
                    numbers_to_log.push_back(current_position.P()[2]);
                    csv30hz.saveDoubles(numbers_to_log);
                }


            }

            servQ.request.target.data = targetQ_rw.toStdVector();

            if(!client.call(servQ))
                ROS_INFO("servo no worko");

        }

        // Out of state machine
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
