#include <fstream>
#include <iostream>
#include <string>
#include <vector>


class csvSaver
{
public:
    csvSaver(std::string filename)
    {
        //fs.open(filename, std::fstream::in | std::fstream::out);
        fs.open(filename, std::ios::out | std::ios::binary);
    }


    void saveDoubles(std::vector<double> data);

private:
    std::ofstream fs;
};
