#include "invkin.h"


invkin::invkin()
{
    // Load workcell:
    _wc = rw::loaders::WorkCellLoader::Factory::load("../catkin_ws/workcells/RopcaDVTScene.xml");
    if (_wc == NULL)
    {

        RW_WARN("Workcell not found.");
        RW_WARN("~/catkin_ws/workcells/RopcaDVTScene.xml");

    }

    // Load robot:
    _robot = _wc->findDevice("UR10");
    if (_robot == NULL)
    {
        RW_WARN("Device UR10 not found");
    }

    _scanPointFrame = _wc->findFrame("ScanPoint");
    if (_scanPointFrame == NULL)
    {
        RW_WARN("probeframe not found");
    }

    rw::math::Q startQ(6, 1.57, -2.4, 2.4, -1.57, -1.57, 0);
    _currentState = _wc->getDefaultState();

    _robot->setQ(startQ,_currentState);
    //urIK = new rw::invkin::JacobianIKSolver(_robot, &(*_scanPointFrame), _currentState);




}

bool invkin::solveIK(rw::math::Transform3D<double> transform, Q &result)
{
    rw::invkin::JacobianIKSolver urIK(_robot, &(*_scanPointFrame), _currentState);
    urIK.setCheckJointLimits(true);
    urIK.setClampToBounds(false);
    urIK.setSolverType(rw::invkin::JacobianIKSolver::JacobianSolverType::SVD);
    urIK.setEnableInterpolation(true);
    urIK.setMaxIterations(1000);

    std::vector<Q> results = urIK.solve(transform, _currentState);

    //std::cout << "Found " << results.size() << " solutions!" << std::endl;

    // Get best solution.
    Q return_val(6, 1.57, -2.4, 2.4, -1.57, -1.57, 0.1);

    if (results.size() > 0)
    {
        //std::cout << "Found " << results.size() << " solutions!" << std::endl;
        result = results[0];
        if(result[5] < -M_PI)
            result[5] += 2*M_PI;
        else if (result[5] > M_PI)
            result[5] -= 2*M_PI;

        _robot->setQ(results[0],_currentState);
        return true;
    }
    else
    {
        RW_WARN("No IK solution found");
        //result = return_val;
        return false;
    }
}


void invkin::solveFK(Q q, rw::math::Transform3D<double> &result)
{
    rw::kinematics::State _fkState = _wc->getDefaultState();
    /*rw::kinematics::Frame::Ptr baseFrame;

    baseFrame = _scanPointFrame = _wc->findFrame("UR10.Base");
    if (baseFrame == NULL)
    {
        RW_WARN("baseFrame");
    }
*/
    _robot->setQ(q,_fkState);

    std::cout << "awesome" <<  _robot->getQ(_fkState) << std::endl;

    result = _robot->baseTframe(&(*_scanPointFrame),_fkState);
    std::cout << "resultssss" << result << std::endl;

}
