#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#include <fstream>
#include <ros/ros.h>

#include <geometry_msgs/Wrench.h>

enum interpType
{
    NEAREST_NEIGHBOUR,
    LINEAR
};

class ForcePath
{
public:
    ForcePath(std::string filename);
    geometry_msgs::Wrench getWrench(double time, interpType type);


//private:
    std::vector<geometry_msgs::Wrench> path;
};
