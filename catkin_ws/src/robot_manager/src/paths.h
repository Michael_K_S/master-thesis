#include <ros/ros.h>
#include <eigen_conversions/eigen_msg.h>


// Msg headers
#include "dvt_processing/coef.h"
#include <std_msgs/Float64.h>
#include <geometry_msgs/Transform.h>

class paths
{
public:
    //paths();
    //~paths();
    void updateVariables(dvt_processing::coef msg);
    geometry_msgs::Transform getTransformLeft(int step);
    geometry_msgs::Transform getTransformRight(int step);
    geometry_msgs::Transform getProxPointLeft(double prox_point_distance, int step);
    geometry_msgs::Transform getProxPointRight(double prox_point_distance, int step);


    Eigen::MatrixXf coefficients1;
    Eigen::MatrixXf coefficients2;
    double start_y;
    double end_y;
    double step_y;

    int polynomial_degree;
};
