#include "csvloader.h"

using namespace std;



ForcePath::ForcePath(std::string filename)
{
    ifstream input_file(filename, ifstream::in);
    geometry_msgs::Wrench currentWrench;
    string currentLine;
    stringstream lineStream;
    double x;

    char del;
    while (input_file.good())
    {
        input_file >> currentWrench.force.x >> del
                >> currentWrench.force.y >> del
                >> currentWrench.force.z >> del
                >> currentWrench.torque.x >> del
                >> currentWrench.torque.y >> del
                >> currentWrench.torque.z >> del;
        path.push_back(currentWrench);
    }

}

geometry_msgs::Wrench ForcePath::getWrench(double time, interpType type)
{
    double step = time * static_cast<double>(path.size()-1);
    if (type == interpType::LINEAR)
    {
        int step_floor = static_cast<int>(floor(step));
        int step_ceil = static_cast<int>(ceil(step));
        double weight =  fmod(step,1.0);
        geometry_msgs::Wrench wrench0 = path[step_floor];
        geometry_msgs::Wrench wrench1 = path[step_ceil];

        geometry_msgs::Wrench ret_val;


        // Calculate return value:
        ret_val.force.x = (1.0 - weight) * wrench0.force.x + weight * wrench1.force.x;
        ret_val.force.y = (1.0 - weight) * wrench0.force.y + weight * wrench1.force.y;
        ret_val.force.z = (1.0 - weight) * wrench0.force.z + weight * wrench1.force.z;

        ret_val.torque.x = (1.0 - weight) * wrench0.torque.x + weight * wrench1.torque.x;
        ret_val.torque.y = (1.0 - weight) * wrench0.torque.y + weight * wrench1.torque.y;
        ret_val.torque.z = (1.0 - weight) * wrench0.torque.z + weight * wrench1.torque.z;

        return ret_val;
    }
    else //(type == interpType::NEAREST_NEIGHBOUR)
    {
        return path[static_cast<int>(round(step))];
    }

}
