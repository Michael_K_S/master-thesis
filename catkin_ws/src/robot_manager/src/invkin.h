#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/models/Device.hpp>
#include <rw/kinematics/Frame.hpp>
#include <rw/kinematics/State.hpp>

#include <rw/loaders/WorkCellLoader.hpp>


#include <rw/invkin/JacobianIKSolver.hpp>
#include <rw/invkin/ClosedFormIKSolverUR.hpp>

#include <vector>
#include <iostream>

using namespace rw::models;
using namespace rw::math;
using namespace rw::kinematics;


class invkin
{
public:
    invkin();
    // Todo: Fix destructor
    ~invkin() {}
    bool solveIK(rw::math::Transform3D<double> transform, Q &result);
    void solveFK(Q q, rw::math::Transform3D<double> &result);




private:
    WorkCell::Ptr _wc;
    Device::Ptr _robot;
    Frame::Ptr _scanPointFrame;
    rw::kinematics::State _currentState;

};



