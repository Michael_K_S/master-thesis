#include "src/paths.h"



void paths::updateVariables(dvt_processing::coef msg)
{
    polynomial_degree = msg.degree;
    coefficients1.resize(polynomial_degree+1,5);
    coefficients2.resize(polynomial_degree+1,5);
    // Flipped these!!!!
    start_y = msg.start_point;
    end_y = msg.end_point;
    step_y = (end_y - start_y)/300;

    // Move data from ros msg to Eigen Matrices
    unsigned int array_size = (polynomial_degree+1)*5;
    for (size_t i = 0; i < array_size; i++)
    {
        coefficients1(i/5,i%5) = msg.coef1[i];
        coefficients2(i/5,i%5) = msg.coef2[i];
        std::cout << msg.coef1[i] << std::endl;
    }
}

geometry_msgs::Transform paths::getTransformLeft(int step)
{
    geometry_msgs::Transform pose;

    pose.translation.y = start_y + static_cast<double>(step)*step_y;


    pose.translation.x = 0;
    pose.translation.z = 0;

    pose.rotation.x = 0;
    pose.rotation.y = 0;
    pose.rotation.z = 0;
    pose.rotation.w = 0;

    for (size_t j = 0; j < coefficients1.rows(); j++)
    {
        pose.translation.x += coefficients1(j,0)*pow(pose.translation.y,j);
        pose.translation.z += coefficients1(j,1)*pow(pose.translation.y,j);

        pose.rotation.x += coefficients1(j,2)*pow(pose.translation.y,j);
        pose.rotation.y += coefficients1(j,3)*pow(pose.translation.y,j);
        pose.rotation.z += coefficients1(j,4)*pow(pose.translation.y,j);
    }
    std::cout << pose << std::endl;
    return pose;
}

geometry_msgs::Transform paths::getTransformRight(int step)
{
    geometry_msgs::Transform pose;

    pose.translation.y = start_y + static_cast<double>(step)*step_y;


    pose.translation.x = 0;
    pose.translation.z = 0;

    pose.rotation.x = 0;
    pose.rotation.y = 0;
    pose.rotation.z = 0;
    pose.rotation.w = 0;

    for (size_t j = 0; j < coefficients2.rows(); j++)
    {
        pose.translation.x += coefficients2(j,0)*pow(pose.translation.y,j);
        pose.translation.z += coefficients2(j,1)*pow(pose.translation.y,j);

        pose.rotation.x += coefficients2(j,2)*pow(pose.translation.y,j);
        pose.rotation.y += coefficients2(j,3)*pow(pose.translation.y,j);
        pose.rotation.z += coefficients2(j,4)*pow(pose.translation.y,j);
    }
    std::cout << pose << std::endl;
    return pose;
}

geometry_msgs::Transform paths::getProxPointLeft(double prox_point_distance, int step)
{

    geometry_msgs::Transform prox_pose = getTransformLeft(step);
    double length = sqrt(pow(prox_pose.rotation.x,2) + pow(prox_pose.rotation.y,2) + pow(prox_pose.rotation.z,2));

    prox_pose.translation.x += prox_pose.rotation.x / length * prox_point_distance;
    prox_pose.translation.y += prox_pose.rotation.y / length * prox_point_distance;
    prox_pose.translation.z += prox_pose.rotation.z / length * prox_point_distance;

    std::cout << prox_pose << std::endl;
    return prox_pose;
}


geometry_msgs::Transform paths::getProxPointRight(double prox_point_distance, int step)
{

    geometry_msgs::Transform prox_pose = getTransformRight(step);

    double length = sqrt(pow(prox_pose.rotation.x,2) + pow(prox_pose.rotation.y,2) + pow(prox_pose.rotation.z,2));

    prox_pose.translation.x += prox_pose.rotation.x / length * prox_point_distance;
    prox_pose.translation.y += prox_pose.rotation.y / length * prox_point_distance;
    prox_pose.translation.z += prox_pose.rotation.z / length * prox_point_distance;

    std::cout << prox_pose << std::endl;
    return prox_pose;
}
