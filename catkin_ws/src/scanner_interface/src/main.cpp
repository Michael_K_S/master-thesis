// ROS
#include <ros/ros.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/Transform.h>
#include <std_msgs/Float64.h>

// Caros control
#include <caros_control_msgs/RobotState.h>
#include <caros_common_msgs/Q.h>
#include <caros/common_robwork.h>

// Robwork
#include <rw/math/Q.hpp>

// Other ros stuff
#include "caros_robovision/pcl.h"

#include "scanner_interface/prepareScan.h"
#include "robot_manager/changeState.h"

// STD libs
#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>

enum  scannerState
{
    PREPARE_SCAN,
    MOVE_TO_BED,
    CALIBRATE_TO_MARKER,
    PREPARE_CHARGE,
    MOVE_TO_CHARGE,
    WAIT
};



geometry_msgs::Pose2D mir_pose_current;
geometry_msgs::Pose2D mir_pose_charging;
geometry_msgs::Pose2D mir_pose_at_bed;
geometry_msgs::Pose2D mir_pose_test;

rw::math::Q dq_rw;
bool urIsMoving = false;
scannerState state = WAIT;





void mir_position_callback(const geometry_msgs::Pose2D::ConstPtr& pose)
{
    mir_pose_current = (*pose);
}


void robot_state_callback(const caros_control_msgs::RobotState state)
{
    //ROS_INFO("CALLBACK");
    dq_rw = caros::toRw(state.dq);
    //std::cout << dq_rw.norm2() << std::endl;
    if (dq_rw.norm2() > 0.01)
        urIsMoving = true;
    else
        urIsMoving = false;


}

bool prepareScan(scanner_interface::prepareScan::Request &req,
               scanner_interface::prepareScan::Response &res)
{
    state = PREPARE_SCAN;
    return true;
}

bool goCharge(scanner_interface::prepareScan::Request &req,
               scanner_interface::prepareScan::Response &res)
{
    state = PREPARE_CHARGE;
    return true;
}


bool mirAtBed()
{
    geometry_msgs::Pose2D offset;
    offset.x = mir_pose_at_bed.x - mir_pose_current.x;
    offset.y = mir_pose_at_bed.y - mir_pose_current.y;
    offset.theta = mir_pose_at_bed.theta - mir_pose_current.theta;
    if (abs(offset.x) < 0.05 && abs(offset.x) < 0.05 && abs(offset.theta) < 0.1)
    {
        std::cout << "offset x: " << offset.x << "offset y: " << offset.y << " offset theta:" << offset.theta << std::endl;
        return true;
    }
    else
        return false;
}

int main(int argc, char** argv) {
    // roscpp
    ros::init(argc, argv, "scanner_interface");
    ros::NodeHandle nh;

    mir_pose_charging.x = -0.05;
    mir_pose_charging.y = 0.32;
    mir_pose_charging.theta = -0.26;

    mir_pose_test.x = 3.27;
    mir_pose_test.y = 0.89;
    mir_pose_test.theta = M_PI_2;

    mir_pose_at_bed.x = 4.71;
    mir_pose_at_bed.y = 2.74;
    mir_pose_at_bed.theta = 1.29-M_PI;


    int counter = 150;


    // Publish topics
    ros::Publisher pub_mir_move = nh.advertise<geometry_msgs::Pose2D>("/mir_interface/control/move_to_position",10);


    // Subscribe to topics
    ros::Subscriber sub_mir_position = nh.subscribe("/mir_interface/current_position",
                                                      10, mir_position_callback);
    ros::Subscriber sub_robotstate = nh.subscribe("/caros_universalrobot/caros_serial_device_service_interface/robot_state",
                                                  10, robot_state_callback);



    // Advertise services
    ros::ServiceServer prepareScan_srv = nh.advertiseService("/ui/prepareScan", prepareScan);
    // Advertise services
    ros::ServiceServer goCharge_srv = nh.advertiseService("/ui/goCharge", goCharge);

    // Setup service clients
    ros::ServiceClient get_pcl_client = nh.serviceClient<caros_robovision::pcl>("rv/get_pcl");
    ros::ServiceClient moveToHome_client = nh.serviceClient<robot_manager::changeState>("rm/moveToHome");
    ros::ServiceClient cameraAtMarker_client = nh.serviceClient<robot_manager::changeState>("rm/cameraAtMarker");


    ROS_INFO("Scanner interface up!");




    ros::spinOnce();

    ros::Rate loop_rate(30);

robot_manager::changeState moveHome_srv;
caros_robovision::pcl pcl_srv;
    while(ros::ok())
    {

        switch(state)
        {
        case PREPARE_SCAN:

            get_pcl_client.call(pcl_srv);

            moveToHome_client.call(moveHome_srv);
            // Wait till UR is done moving.
            ros::Duration(0.5).sleep();
            state = MOVE_TO_BED;
            break;
        case MOVE_TO_BED:
            if (!urIsMoving)
            {
                pub_mir_move.publish(mir_pose_at_bed);

                state = CALIBRATE_TO_MARKER;
            }
            break;

        case CALIBRATE_TO_MARKER:
            if (mirAtBed())
            {
                ROS_INFO("ARRIVED AT BED");
                cameraAtMarker_client.call(moveHome_srv);
                state = WAIT;
            }
            break;
        case PREPARE_CHARGE:
            moveToHome_client.call(moveHome_srv);
            // Wait till UR is done moving.
            if (counter-- == 0)
            {
                state = MOVE_TO_CHARGE;
                counter = 100;
            }
                break;

        case MOVE_TO_CHARGE:
            if (!urIsMoving)
            {
                pub_mir_move.publish(mir_pose_charging);
                state = WAIT;
            }
            break;


    }

        ros::spinOnce();
        loop_rate.sleep();

    }


    return 0;



}
