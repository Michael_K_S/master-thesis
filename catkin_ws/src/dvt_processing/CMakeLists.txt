cmake_minimum_required(VERSION 2.8.3)
project(dvt_processing)

#Catkin:
find_package(catkin REQUIRED roscpp std_msgs sensor_msgs pcl_ros message_generation)



set(node_name "${PROJECT_NAME}_node")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(CMAKE_INCLUDE_CURRENT_DIR ON)


#set (pcl_processor_ROOT "~/Dokumenter/master-thesis/pcl_processor")
#find_package(pcl_processor REQUIRED)


# Import pcl_processor lib
add_library(pcl_processor SHARED IMPORTED)
set_property(TARGET pcl_processor PROPERTY IMPORTED_LOCATION "/usr/local/lib/libpcl_processor.so")

# Add homegrown libs:
add_library(path_estimator SHARED IMPORTED)
set_property(TARGET path_estimator PROPERTY IMPORTED_LOCATION "/usr/local/lib/libpath_estimator.so")

#VTK:

find_package (VTK REQUIRED)
include(${VTK_USE_FILE})



#Find awesome openMP
find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()


#add_service_files(
#    FILES
#    pcl.srv
#)

add_message_files(
	FILES
	coef.msg
)


generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
    CATKIN_DEPENDS message_runtime
)



include_directories(
	${catkin_INCLUDE_DIRS} 
)
add_definitions(
	${catkin_DEFINITIONS}
#        ${PCL_DEFINITIONS}
)





add_executable(${node_name} "src/main.cpp")

target_link_libraries(${node_name} 
		${catkin_LIBRARIES}
                ${VTK_LIBRARIES}
		pcl_processor
                path_estimator
)


