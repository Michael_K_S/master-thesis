// ROS
#include <ros/ros.h>

#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <eigen_conversions/eigen_msg.h>

// Homegrown libs for path estimation
#include <pclprocessor.h>
#include <path_estimator.h>

// Homemade msg header
#include "dvt_processing/coef.h"
#include <std_msgs/Float64.h>

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/passthrough.h>


// STD libs
#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>

PCLProcessor processor;

// Pointer to publisher
ros::Publisher* pubptr;

void pclReceivedCallback(const sensor_msgs::PointCloud2::ConstPtr& pcl)
{
    PointCloudT::Ptr cloud (new PointCloudT);

    ROS_INFO("RECEIVED A PCL!");
    pcl::fromROSMsg(*pcl, *cloud);

    /*pcl::PassThrough<pcl::PointXYZRGB> ptfilter (true);
    ptfilter.setInputCloud (cloud);
    // Filter Z
    ptfilter.setFilterFieldName ("z");
    ptfilter.setFilterLimits (0.01, 2);
    ROS_INFO("Starting filter");
    ptfilter.filter (*cloud);
    ROS_INFO("Filtering done");*/

    std::cout << cloud->points.size() << std::endl;

    // RANSAC:
    Eigen::VectorXf plane_parameters;
    int inlier_count;
    PointCloudT::Ptr cloud_ransac (new PointCloudT);
    processor.fitPlane(cloud,cloud_ransac,0.01,plane_parameters,inlier_count);
    


    Eigen::Matrix4f transform;
    double c,s;
    Eigen::Vector3f axis_rot;
    processor.cropFromPlane(cloud,plane_parameters,transform,axis_rot,c,s);


    Eigen::Vector3f plane_normal;
    plane_normal.x() = plane_parameters.x();
    plane_normal.y() = plane_parameters.y();
    plane_normal.z() = plane_parameters.z();

    std::cout << "Plane params: " << plane_parameters << std::endl;
    ROS_INFO("RANSAC!!!!");
    return;

    // KMEANS:
    PointCloudT::Ptr cluster1 (new PointCloudT);
    PointCloudT::Ptr cluster2 (new PointCloudT);
    Eigen::Vector3f centroid1;
    Eigen::Vector3f centroid2;
    processor.KMeansClustering(cloud,cluster1,cluster2,centroid1,centroid2);
    ROS_INFO("kmeans clustering done");

    // SPLIT the clusters based on the centroids:
#pragma omp parallel sections
    {
#pragma omp section
        processor.splitPC(cluster1,centroid1,centroid2);
#pragma omp section
        processor.splitPC(cluster2,centroid2,centroid1);
    }
    ROS_INFO("split of clusters done"); 

    // Estimate normals
    pcl::PointCloud<pcl::Normal>::Ptr cluster1_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<pcl::Normal>::Ptr cluster2_normals (new pcl::PointCloud<pcl::Normal>);


    ROS_INFO("Removing outliers...");
#pragma omp parallel sections
{
#pragma omp section
        {
    processor.removeOutliers(cluster1, 3, 2.0);
    processor.removeNanPoints(cluster1);
        }
#pragma omp section
        {
    processor.removeOutliers(cluster2, 3, 2.0);
    processor.removeNanPoints(cluster2);
        }
}
    ROS_INFO("Removing outliers... DONE!");

    processor.estimateNormals(cluster1,cluster1_normals);
    processor.estimateNormals(cluster2,cluster2_normals);

    std::cout << "cluster 1 size / normals: " << cluster1->points.size()  << " / " << cluster1_normals->points.size() << std::endl;
    std::cout << "cluster 2 size / normals: " << cluster2->points.size()  << " / " << cluster2_normals->points.size() << std::endl;


    // Crop pointclouds
    PointCloudT::Ptr cluster1_scanpoints (new PointCloudT);
    PointCloudT::Ptr cluster2_scanpoints (new PointCloudT);


    double min_angle = 40;
    double max_angle = 50;
#pragma omp parallel sections
{
#pragma omp section
    processor.filterNormals(cluster1,cluster1_normals,cluster1_scanpoints,plane_normal,min_angle/180.0*M_PI, max_angle/180.0*M_PI);
#pragma omp section
    processor.filterNormals(cluster2,cluster2_normals,cluster2_scanpoints,plane_normal,min_angle/180.0*M_PI, max_angle/180.0*M_PI);
}


    std::cout << "cluster 1 size / normals: " << cluster1->points.size()  << " / " << cluster1_normals->points.size() << std::endl;
    std::cout << "cluster 2 size / normals: " << cluster2->points.size()  << " / " << cluster2_normals->points.size() << std::endl;


    ROS_INFO("done with pcl operations");

    // Estimate scan paths start and end points
    double start1, end1, start2, end2;
    std::cout << cluster1->points.size() << std::endl;
    path_estimator::getEndpoints(cluster1_scanpoints,start1,end1);
    path_estimator::getEndpoints(cluster2_scanpoints,start2,end2);


    // Start points estimated
    std::cout << start1 << " - " << end1 << std::endl;
    std::cout << start2 << " - " << end2 << std::endl;

    start1 = std::max(start1, start2);
    end1 = std::min(end1, end2);


    // Debug scanpoints:
    std::cout << "Scanpoints1 count: " << cluster1_scanpoints->points.size() << std::endl;
    std::cout << "Scanpoints2 count: " << cluster2_scanpoints->points.size() << std::endl;
    pcl::PCDWriter writer;
    writer.write<PointT> ("scanpoints1", *cluster1_scanpoints, false);
    writer.write<PointT> ("scanpoints2", *cluster2_scanpoints, false);

    writer.write<pcl::Normal> ("scanpoints1normal", *cluster1_normals, false);
    writer.write<pcl::Normal> ("scanpoints2normal", *cluster2_normals, false);


    // Estimate path coefficients (should rely on parameter server)
    Eigen::MatrixXf coefficients1;
    Eigen::MatrixXf coefficients2;



    unsigned int polynomial_deg = 1;

    coefficients1 = path_estimator::fitPolynomialLS(cluster1_scanpoints, cluster1_normals, polynomial_deg);

    std::cout << "first polynomial estimated" << std::endl;

    coefficients2 = path_estimator::fitPolynomialLS(cluster2_scanpoints, cluster2_normals, polynomial_deg);

    std::cout << "Coefficients1: " << coefficients1 << std::endl;
    std::cout << "Coefficients2: " << coefficients2 << std::endl;


    ROS_INFO("All processing done!");
    std::cout << start1 << " - " << end1 << std::endl;


    // Back stuff into ros msg and send it :)
    dvt_processing::coef result_msg;
    result_msg.degree = polynomial_deg;
    result_msg.start_point = start1;
    result_msg.end_point = end1;

    unsigned int array_size = (polynomial_deg+1)*5;
    std_msgs::Float64 array1[array_size];
    std_msgs::Float64 array2[array_size];


    std::cout << coefficients1.rows() << " ooog " << coefficients1.cols() << std::endl;
    for (size_t i = 0; i < array_size; i++)
    {
        std::cout << "Checking coef: " << i/5 << " , " << i%5 << std::endl;
        result_msg.coef1.push_back(static_cast<double>(coefficients1(i/5,i%5)));
        result_msg.coef2.push_back(static_cast<double>(coefficients2(i/5,i%5)));
    }

    //result_msg.coef1 = array1;
    //result_msg.coef2 = array2;
    pubptr->publish(result_msg);


}


int main(int argc, char** argv) {
    // roscpp
    ros::init(argc, argv, "dvt_processing_node");
    ros::NodeHandle nh;

    ros::Subscriber sub = nh.subscribe("/points2",1,pclReceivedCallback);

    ros::Publisher pub_coef1 = nh.advertise<dvt_processing::coef> ("coefficients", 1);
    pubptr = &pub_coef1;
    dvt_processing::coef empty_msg;

    empty_msg.degree = 1;

    ros::spin();


    return 0;



}
