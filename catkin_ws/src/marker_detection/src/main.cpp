// ROS
#include <ros/ros.h>
#include <geometry_msgs/Transform.h>


// Caros
#include <caros/common_robwork.h>

// RW:
#include <rw/math/Transform3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/math/Q.hpp>



// STD libs
#include <memory>
#include <iostream>
#include <vector>
#include <string>

#include <cstdlib>

#include "XmlRpc.h"
//#include "xmlrpcpp/XmlRpcClient.h"

#include "marker_detection/detectMarker.h"

using namespace std;
using namespace XmlRpc;

rw::math::Transform3D<double> baseTsensor;
rw::math::Transform3D<double> baseTmarker;

// Setup the transform from marker to cam.

//rw::math::RPY<double> markerRcam(0,-M_PI_2,0);
rw::math::RPY<double> markerRcam(0,M_PI_2,-M_PI);


// 0,0,0.175
// 0,0,0.195

rw::math::Transform3D<double> markerTcam(rw::math::Vector3D<double>(-0.06,0,-0.03),markerRcam.toRotation3D());



rw::math::Q targetQ_rw(6, 0.0, -2.09, 1.74, -2.82, -1.57, 1.57);


bool detectMarker(marker_detection::detectMarker::Request &req,
                  marker_detection::detectMarker::Response &res)
{
    ROS_INFO("in service call");
    XmlRpcClient client("192.168.12.100", 4567);
    XmlRpcValue args, r;
    args[0] = "RobotBase";
    args[1]["q1"] = targetQ_rw[0];
    args[1]["q2"] = targetQ_rw[1];
    args[1]["q3"] = targetQ_rw[2];
    args[1]["q4"] = targetQ_rw[3];
    args[1]["q5"] = targetQ_rw[4];
    args[1]["q6"] = targetQ_rw[5];

    //
    if (client.execute("GetMarkerInFieldOfView",args,r))
    {
        //std::cout << r << std::endl;
        baseTmarker.R() = rw::math::Rotation3D<double>(r[0]["r1"], r[0]["r2"], r[0]["r3"],
                                                       r[0]["r4"], r[0]["r5"], r[0]["r6"],
                                                       r[0]["r7"], r[0]["r8"], r[0]["r9"]);




        baseTmarker.P() = rw::math::Vector3D<double>(r[0]["x"], r[0]["y"], r[0]["z"]);

        std::cout << "baseTmarker estimate:" << std::endl;
        std::cout << baseTmarker << std::endl;

        baseTsensor = baseTmarker*markerTcam;
        res.baseTsensor = caros::toRos(baseTsensor);
        return true;
    }
    else
    {
        std::cout << "error" << std::endl;
    }

    return false;
}


int main(int argc, char** argv) {
    // roscpp
    ros::init(argc, argv, "marker_detection");
    ros::NodeHandle nh;

    // Advertise services
    ros::ServiceServer detectMarker_srv = nh.advertiseService("md/detectMarker", detectMarker);


    ROS_INFO("Marker detection operational");


    ros::spin();

    return 0;



}
