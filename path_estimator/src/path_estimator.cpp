#include "path_estimator.h"

Eigen::MatrixXf path_estimator::fitPolynomialLS(const PointCloudT::Ptr &input_cloud, const pcl::PointCloud<pcl::Normal>::Ptr &cloud_normals, unsigned int polynomial_degree)
{

    // Setup variables needed for least squares: 2d polynomial
    // Form: Ax = b, least squares: x = (A'*A)^-1 * A' * b
    int size = input_cloud->points.size();
    Eigen::MatrixXf A(size,polynomial_degree+1);
    Eigen::MatrixXf b(size,5); // constant
    Eigen::Vector3f current_point, current_normal;


    // Put some content in A and b (iterate through all points)
    for (size_t i = 0; i < size; i++)
    {
        current_point = (*input_cloud)[i].getVector3fMap();
        current_normal = (*cloud_normals)[i].getNormalVector3fMap();
        for (size_t j = 0; j < polynomial_degree+1; j++)
        {
            A(i,j) = pow(current_point.y(),j);

        }
        // b, row: x z a b c
        b(i,0) = current_point.x();
        b(i,1) = current_point.z();
        b(i,2) = current_normal.x();
        b(i,3) = current_normal.y();
        b(i,4) = current_normal.z();
    }
    // Solve for x:
    //std::cout << A << std::endl;
/*
    std::cout << "a size: " << std::endl;
    std::cout << A.rows() << " - " << A.cols() << std::endl;
    std::cout << A << std::endl;
    std::cout << "b: \n"  << std::endl;
    std::cout << b.rows() << " - " << b.cols() << std::endl;
    //std::cout << b << std::endl;



    std::cout << " A transpose size: " << std::endl;
    std::cout << At.rows() << " - " << At.cols() << std::endl;
    //std::cout << At<< std::endl;

    Eigen::MatrixXf squared = At*A;
    std::cout << " squared" << std::endl;
    std::cout << squared.rows() << " - " << squared.cols() << std::endl;

    Eigen::MatrixXf inv = squared.inverse();
    std::cout << " inverted" << std::endl;
    std::cout << inv.rows() << " - " << inv.cols() << std::endl;


    Eigen::MatrixXf invAt = inv*At;
    std::cout << " inverted and multiplyied" << std::endl;

    invAt*b;
    std::cout << " invAt" << std::endl;

    Eigen::MatrixXf result =
    std::cout << "result: " << std::endl;
    std::cout << result << std::endl;


*/
    return (A.transpose()*A).inverse()*A.transpose()*b;


}

void path_estimator::getEndpoints(const PointCloudT::Ptr &input_cloud, double &start_y, double &end_y)
{
    PointT min_point, max_point;
    pcl::getMinMax3D(*input_cloud, min_point, max_point);
    start_y = (double) min_point.y;
    end_y = (double) max_point.y;
}
