#ifndef PATH_ESTIMATOR_H
#define PATH_ESTIMATOR_H

#include <eigen3/Eigen/Dense>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>

// This should not be defined multiple times.
typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

namespace path_estimator
{
/**
 * @brief fitPolynomialLS fit a polynomial to the points in the input cloud
 * @param input_cloud the point cloud to fit a line to
 * @param polynomial_degree degree of the polynomial
 * @return
 */
Eigen::MatrixXf fitPolynomialLS(const PointCloudT::Ptr &input_cloud, const pcl::PointCloud<pcl::Normal>::Ptr &cloud_normals, unsigned int polynomial_degree = 2);

/**
 * @brief getEndpoints
 * @param start_y
 * @param end_y
 * @return
 */
void getEndpoints(const PointCloudT::Ptr &input_cloud, double &start_y, double &end_y);

}
#endif // PATH_ESTIMATOR_H
