#-------------------------------------------------
#
# Project created by QtCreator 2014-05-01T14:24:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets

TARGET = pcl_visualizer
TEMPLATE = app
CONFIG += c++11 \
            openmp

SOURCES += main.cpp\
        pclviewer.cpp

HEADERS  += pclviewer.h

FORMS    += pclviewer.ui


QMAKE_CXXFLAGS_DEBUG += -fopenmp
QMAKE_CXXFLAGS_RELEASE += -fopenmp
QMAKE_LFLAGS_DEBUG += -fopenmp
QMAKE_LFLAGS_RELEASE += -fopenmp

LIBS += -fopenmp


INCLUDEPATH += /usr/local/include/pcl-1.8
