#include "pclviewer.h"
#include "../build/ui_pclviewer.h"
#include <pcl/filters/passthrough.h>
#include <pcl/common/common.h>
#include <pcl/io/pcd_io.h>


#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

PCLViewer::PCLViewer (QWidget *parent) :
  QMainWindow (parent),
  ui (new Ui::PCLViewer)
{
  ui->setupUi (this);
  this->setWindowTitle ("PCL viewer");

  cloud.reset (new PointCloudT);
  cloud_cropped.reset (new PointCloudT);
  cloud_ransac.reset (new PointCloudT);
  cluster1.reset (new PointCloudT);
  cluster2.reset (new PointCloudT);
  cluster1_normals.reset (new pcl::PointCloud<pcl::Normal>);
  cluster2_normals.reset (new pcl::PointCloud<pcl::Normal>);
  cluster1_scanpoints.reset (new PointCloudT);
  cluster2_scanpoints.reset (new PointCloudT);




  // Set up the QVTK window
  viewer.reset (new pcl::visualization::PCLVisualizer ("viewer", false));
  ui->qvtkWidget->SetRenderWindow (viewer->getRenderWindow ());
  viewer->setupInteractor (ui->qvtkWidget->GetInteractor (), ui->qvtkWidget->GetRenderWindow ());
  ui->qvtkWidget->update ();



  viewer->addPointCloud (cloud, "cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud");
  viewer->addCoordinateSystem();

  viewer->resetCamera ();
  viewer->setCameraPosition(0,0,0,0,0,1,0);
  ui->qvtkWidget->update ();


  ui->xmin_spinBox->setRange(-1,1);
  ui->xmax_spinBox->setRange(-1,1);


  ui->ymin_spinBox->setRange(-1,1);
  ui->ymax_spinBox->setRange(-1,1);

  ui->zmin_spinBox->setRange(0,2);
  ui->zmax_spinBox->setRange(0,2);

}

void PCLViewer::setSpinBoxValues()
{
    PointT min_point, max_point;
    pcl::getMinMax3D(*cloud, min_point, max_point);

    ui->xmin_spinBox->setValue(min_point.x);
    ui->xmax_spinBox->setValue(max_point.x);
    ui->ymin_spinBox->setValue(min_point.y);
    ui->ymax_spinBox->setValue(max_point.y);
    ui->zmin_spinBox->setValue(0.001);
    ui->zmax_spinBox->setValue(max_point.z);
}

void PCLViewer::Log(std::string msg)
{
    QString current_log = ui->plainTextEdit->toPlainText();
    current_log.append("\n");
    current_log.append(msg.c_str());
    ui->plainTextEdit->setPlainText(current_log);
    ui->plainTextEdit->verticalScrollBar()->setSliderPosition(ui->plainTextEdit->verticalScrollBar()->maximum());

}
void PCLViewer::setColor(PointCloudT::Ptr &cloud, int r, int g, int b)
{
    // Fill the cloud with some points
      for (size_t i = 0; i < cloud->points.size (); ++i)
      {
        cloud->points[i].r = r;
        cloud->points[i].g = g;
        cloud->points[i].b = b;
      }
}


PCLViewer::~PCLViewer ()
{
  delete ui;
}

void PCLViewer::update_view()
{
    // Update this so it updates all added pointclouds (we might need some wrapper structure)
    viewer->updatePointCloud(cloud, "cloud");
    ui->qvtkWidget->update ();
}

void PCLViewer::on_crop_pushButton_clicked()
{
    processor.crop(cloud, ui->xmin_spinBox->value(), ui->xmax_spinBox->value(),
                   ui->ymin_spinBox->value(), ui->ymax_spinBox->value(),
                    ui->zmin_spinBox->value(), ui->zmax_spinBox->value());

    update_view();
    Log("Cropped pointcloud");
}

void PCLViewer::on_actionLoad_triggered()
{
    fileName = QFileDialog::getOpenFileName(this, "Open the file");
    on_actionReload_triggered();
    Log("File loaded!");
}

void PCLViewer::on_actionReload_triggered()
{
    reader.read<PointT>(fileName.toStdString(), *cloud);
    //setColor(cloud,100,100,100);
    update_view();
    setSpinBoxValues();
    on_crop_pushButton_clicked();
    processor.removeNanPoints(cloud);
    //processor.removeZeroPoints(cloud);
}

void PCLViewer::on_actionSave_triggered()
{
    QString fileNameSave = QFileDialog::getSaveFileName(this, "Saves as");
    writer.write<PointT> (fileNameSave.toStdString(), *cloud, false);

    writer.write<PointT> ("cluster1.pcd", *cluster1, false);
    writer.write<PointT> ("cluster2.pcd", *cluster2, false);

    writer.write<PointT> ("scanpoints1_scanpoints.pcd", *cluster1_scanpoints, false);
    writer.write<PointT> ("scanpoints2_scanpoints.pcd", *cluster2_scanpoints, false);

    Log("Saved pcl at: ");
    Log(fileNameSave.toStdString());
}

void PCLViewer::on_downsample_pushButton_clicked()
{
    // Downsample the cloud (speeds up other operations)
    processor.downsample(cloud,ui->downsample_spinBox->value());
    update_view();
    std::stringstream msg;
    msg << "Downsampled to: " << cloud->points.size() << " points.";
    Log(msg.str());
}



void PCLViewer::on_ransac_pushButton_clicked()
{
    Eigen::VectorXf plane_parameters;
    int inlier_count;
    std::cout << "calling function";
    processor.fitPlane(cloud,cloud_ransac,ui->ransac_spinBox->value(),plane_parameters,inlier_count);
    std::cout << "finction returned";
    std::stringstream msg;
    if (ui->ransac_checkBox->isChecked())
    {
        // Add plane to viewer
        setColor(cloud_ransac,100,0,0);
        //setColor(cloud,100,100,0);
        viewer->addPointCloud(cloud_ransac,"cloud_ransac");
        update_view();
    }
    msg << "Fitted plane with parameters: " << plane_parameters << "\n number of inliers: "
        << cloud_ransac->points.size() << "\n numberof outliers: " << cloud->points.size();
    Log(msg.str());

    Eigen::Matrix4f transform;
    double c,s;
    Eigen::Vector3f axis_rot;
    processor.cropFromPlane(cloud,plane_parameters,transform,axis_rot,c,s);
    msg.clear();
    msg << cloud->points.size() << "\n\n" << transform << "\n" << axis_rot << "\n" << c << "\n" << s;
    Log(msg.str());

    plane_normal.x() = plane_parameters.x();
    plane_normal.y() = plane_parameters.y();
    plane_normal.z() = plane_parameters.z();

    //setColor(cloud,0,0,100);
    //viewer->addPointCloud(cloud,"cloud1");

    update_view();

}


void PCLViewer::on_actionSave_bedplane_pcd_triggered()
{
    QString fileNameSave = QFileDialog::getSaveFileName(this, "Saves as");
    writer.write<PointT> (fileNameSave.toStdString(), *cloud_ransac, false);
    Log("Saved pcl at: ");
    Log(fileNameSave.toStdString());
}



void PCLViewer::on_uncertain_pushButton_clicked()
{
    processor.removeUncertainPoints(cloud, ui->uncertain_spinBox->value());
    update_view();
}

void PCLViewer::on_kmean_pushButton_clicked()
{

    processor.KMeansClustering(cloud,cluster1,cluster2,centroid1,centroid2);
    // Recolor the pointclouds

    setColor(cluster1,0,255,0);
    setColor(cluster2,0,0,255);

    std::stringstream msg;
    msg << "Divided pointcloud into 2 clusters with centroids: \n" << centroid1 << " \n and \n " << centroid2;
    Log(msg.str());

}

void PCLViewer::on_checkBox_cluster1_toggled(bool checked)
{
    if (checked)
    {
        viewer->addPointCloud(cluster1,"cluster1");
    }
    else
    {
        viewer->removePointCloud("cluster1");
    }
    ui->qvtkWidget->update ();
}

void PCLViewer::on_checkBox_cluster2_toggled(bool checked)
{
    if (checked)
    {
        viewer->addPointCloud(cluster2,"cluster2");

    }
    else
    {
        viewer->removePointCloud("cluster2");
    }
    ui->qvtkWidget->update ();
}

void PCLViewer::on_checkBox_cloud_toggled(bool checked)
{
    if (checked)
    {
        viewer->addPointCloud(cloud,"cloud");

    }
    else
    {
        viewer->removePointCloud("cloud");
    }
    ui->qvtkWidget->update ();
}

void PCLViewer::on_checkBox_plane_toggled(bool checked)
{
    if (checked)
    {
        viewer->addPointCloud(cloud_ransac,"cloud_ransac");
    }
    else
    {
        viewer->removePointCloud("cloud_ransac");
    }
    ui->qvtkWidget->update ();
}

void PCLViewer::on_splitclusters_pushButton_clicked()
{
#pragma omp parallel sections
    {
#pragma omp section
    processor.splitPC(cluster1,centroid1,centroid2);
#pragma omp section
    processor.splitPC(cluster2,centroid2,centroid1);
    }
}

void PCLViewer::on_actionReset_triggered()
{
    cloud.reset (new PointCloudT);
    cloud_cropped.reset (new PointCloudT);
    cloud_ransac.reset (new PointCloudT);
    cluster1.reset (new PointCloudT);
    cluster2.reset (new PointCloudT);
    
}

void PCLViewer::on_normals_pushButton_clicked()
{
   processor.estimateNormals(cluster1,cluster1_normals);
   processor.estimateNormals(cluster2,cluster2_normals);
}

void PCLViewer::on_filternormals_pushButton_clicked()
{
    double min_angle = ui->minnormal_spinBox->value();
    double max_angle = ui->maxnormal_spinBox->value();


#pragma omp parallel sections
{
#pragma omp section
    processor.filterNormals(cluster1,cluster1_normals,cluster1_scanpoints,plane_normal,min_angle/180*M_PI, max_angle/180*M_PI);
#pragma omp section
    processor.filterNormals(cluster2,cluster2_normals,cluster2_scanpoints,plane_normal,min_angle/180*M_PI, max_angle/180*M_PI);
}


    ui->checkBox_scanpoints1->setChecked(false);
    ui->checkBox_scanpoints1->setChecked(true);

    ui->checkBox_scanpoints2->setChecked(false);
    ui->checkBox_scanpoints2->setChecked(true);
}
void PCLViewer::on_checkBox_scanpoints1_toggled(bool checked)
{
    if (checked)
    {
        viewer->addPointCloud(cluster1_scanpoints, "scanpoints1");
    }
    else
    {
        viewer->removePointCloud("scanpoints1");
    }
    ui->qvtkWidget->update ();
}

void PCLViewer::on_checkBox_scanpoints2_toggled(bool checked)
{
    if (checked)
    {
        viewer->addPointCloud(cluster2_scanpoints, "scanpoints2");
    }
    else
    {
        viewer->removePointCloud("scanpoints2");
    }
    ui->qvtkWidget->update ();
}

void PCLViewer::on_lsq_pushButton_clicked()
{
    double start, end;
    path_estimator::getEndpoints(cluster1_scanpoints,start,end);

    Eigen::MatrixXf coefficients = path_estimator::fitPolynomialLS(cluster1_scanpoints, cluster1_normals, 1);

    std::cout << "Start: " << start << " End: " << end << std::endl;
    std::cout << "Coefficients: \n" << coefficients << std::endl;


    Eigen::MatrixXf coefficients2 = path_estimator::fitPolynomialLS(cluster2_scanpoints, cluster2_normals, 1);

    std::cout << "Start: " << start << " End: " << end << std::endl;
    std::cout << "Coefficients2s: \n" << coefficients2 << std::endl;
}
