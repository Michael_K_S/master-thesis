#ifndef PCLVIEWER_H
#define PCLVIEWER_H

#include <iostream>

// Qt
#include <QMainWindow>
#include <QFile>
#include <QScrollBar>

// Point Cloud Library
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>

// Visualization Toolkit (VTK)
#include <vtkRenderWindow.h>

#include "pclprocessor.h"
#include "path_estimator.h"
#include <string>
#include <sstream>
namespace Ui
{
class PCLViewer;
}

class PCLViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit PCLViewer (QWidget *parent = 0);
    ~PCLViewer ();
    PCLProcessor processor;
    void Log(std::string msg);


protected:
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    PointCloudT::Ptr cloud;
    PointCloudT::Ptr cloud_cropped;
    PointCloudT::Ptr cloud_ransac;
    PointCloudT::Ptr cluster1;
    pcl::PointCloud<pcl::Normal>::Ptr cluster1_normals;
    PointCloudT::Ptr cluster2;
    pcl::PointCloud<pcl::Normal>::Ptr cluster2_normals;
    PointCloudT::Ptr cluster1_scanpoints;
    PointCloudT::Ptr cluster2_scanpoints;
    Eigen::Vector3f centroid1;
    Eigen::Vector3f centroid2;
    Eigen::Vector3f plane_normal;
    void update_view();



private slots:
    void on_actionLoad_triggered();


    void on_actionReload_triggered();

    void on_actionSave_triggered();

    void on_downsample_pushButton_clicked();

    void on_ransac_pushButton_clicked();

    void on_crop_pushButton_clicked();

    void on_actionSave_bedplane_pcd_triggered();



    void on_uncertain_pushButton_clicked();

    void on_kmean_pushButton_clicked();

    void on_checkBox_cluster1_toggled(bool checked);

    void on_checkBox_cluster2_toggled(bool checked);

    void on_checkBox_cloud_toggled(bool checked);

    void on_checkBox_plane_toggled(bool checked);

    void on_splitclusters_pushButton_clicked();

    void on_actionReset_triggered();

    void on_normals_pushButton_clicked();

    void on_filternormals_pushButton_clicked();

    void on_checkBox_scanpoints1_toggled(bool checked);

    void on_checkBox_scanpoints2_toggled(bool checked);

    void on_lsq_pushButton_clicked();

private:
    Ui::PCLViewer *ui;
    void setSpinBoxValues();
    void setColor(PointCloudT::Ptr &cloud, int r, int g, int b);
    QString fileName;
    pcl::PCDReader reader;
    pcl::PCDWriter writer;


};

#endif // PCLVIEWER_H
